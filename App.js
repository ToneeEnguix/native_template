import React, { useCallback, useEffect, useState } from 'react';
import * as SplashScreen from 'expo-splash-screen';
import LoadingScreen from './Loaders/SplashScreen.js';
import Router from './Router';
import { StatusBar } from 'expo-status-bar';
import { View, Platform, SafeAreaView } from 'react-native';
import Constants from 'expo-constants';
import 'react-native-gesture-handler';

export default function App() {
  const [appIsReady, setAppIsReady] = useState(false);
  const [isAndroid, setIsAndroid] = useState(null);

  useEffect(() => {
    // get operating system using Platform by react-native
    const getOperatingSystem = () => {
      Platform.OS === 'ios' ? setIsAndroid(false) : setIsAndroid(true);
    };
    getOperatingSystem();
    async function prepare() {
      try {
        // Keep default splash screen visible while we fetch resources
        // await SplashScreen.preventAutoHideAsync();
        // Pre-load fonts, make any API calls you need to do here
        // In this case we simulate waiting time
        await new Promise((resolve) => setTimeout(resolve, 2000));
      } catch (e) {
        console.warn(e);
      } finally {
        // Tell the application to render
        setAppIsReady(true);
      }
    }
    prepare();
  }, []);

  const onLayoutRootView = useCallback(
    async () => {
      if (appIsReady) {
        // This tells the splash screen to hide immediately! If we call this after
        // `setAppIsReady`, then we may see a blank screen while the app is
        // loading its initial state and rendering its first pixels. So instead,
        // we hide the splash screen once we know the root view has already
        // performed layout.
        await SplashScreen.hideAsync();
      }
    },
    [appIsReady]
  );

  // if app is not ready to display, show splash screen
  if (!appIsReady) {
    return <LoadingScreen />;
  } else {
    return (
      <View style={{ flex: 1 }} onLayout={onLayoutRootView}>
        <StatusBar style="dark" />
        {/* If system is android use Constants to get statusBarHeight and add it as a marginTop to render inside limits */}
        {isAndroid ? (
          <View style={{ marginTop: Constants.statusBarHeight }}>
            <Router />
          </View>
        ) : (
          // If it's iOS use SafeAreaView to render inside limits

          <SafeAreaView style={{ flex: 1 }}>
            <Router />
          </SafeAreaView>


        )}
      </View>
    );
  }
}
