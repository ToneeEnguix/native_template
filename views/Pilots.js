import React from 'react'
import { View, Text, StyleSheet, ScrollView } from "react-native";
import { LinearGradient } from 'expo-linear-gradient';

export default function Pilots({ drivers }) {
  return (
    <View style={styles.wrapper}>
      <LinearGradient
        colors={['#ffffff', '#ffb7b7', '#ff9999']}
        style={styles.container}>
        <Text style={styles.title}>Drivers Standings</Text>
        <ScrollView>
          {drivers.map((pilot, i) => (
            <View key={pilot.Driver?.driverId} style={styles.driverRow}>
              <Text style={styles.position}>{pilot.position}</Text>
              <Text style={styles.driver}>{pilot.Driver?.givenName + " " + pilot.Driver?.familyName}</Text>
              <View style={styles.points}>
                <Text style={{ marginRight: 10, fontSize: 20 }}>{pilot.points}</Text>
                <Text style={{}}>{i !== 0 && `(+${(drivers[0].points - pilot.points).toFixed(1)})`}</Text>
              </View>
            </View>
          ))}
        </ScrollView>
      </LinearGradient>
    </View>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1
  },
  container: {
    flex: 1,
    padding: "5%"
  },
  title: {
    fontSize: 25,
    marginBottom: "4%",
  },
  driverRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: "5%",
  },
  position: {
    width: "7%",
    fontSize: 20,
    textAlign: "right",
    marginRight: "3%",
  },
  driver: {
    width: "60%",
    fontSize: 20,
  },
  points: {
    flexDirection: "row",
    width: "30%",
    justifyContent: "flex-end",
    alignItems: "center",
  }
})
