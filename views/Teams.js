import React from 'react'
import { View, Text, StyleSheet } from "react-native";
import { LinearGradient } from 'expo-linear-gradient';

export default function Teams(props) {
  return (
    <View style={styles.wrapper}>
      <LinearGradient
        colors={['#ffffff', '#ffb7b7', '#ff9999']}
        style={styles.container}>
        <Text style={styles.title}>Constructors Standings</Text>
        <View style={styles.mapWrapper}>
          {props.constructors?.map(team => (
            <View style={styles.teamRow} key={team.Constructor.constructorId}>
              <Text style={styles.position}>{team.position}</Text>
              <Text style={styles.team}>{team.Constructor.name}</Text>
              <Text style={styles.points}>{team.points}</Text>
            </View>
          ))}
        </View>
      </LinearGradient>
    </View>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1
  },
  container: {
    flex: 1,
    padding: "5%"
  },
  title: {
    fontSize: 25,
    marginBottom: "5%",
  },
  mapWrapper: {
    justifyContent: "space-around"
  },
  teamRow: {
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: "5%"
  },
  position: {
    width: "8%",
    fontSize: 20,
    textAlign: "right",
    marginRight: "7%"
  },
  team: {
    width: "50%",
    fontSize: 20
  },
  points: {
    width: "30%",
    fontSize: 20,
    textAlign: "right"
  }
})
