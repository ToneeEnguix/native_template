import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Home from './views/Home';
import Pilots from './views/Pilots';
import Teams from './views/Teams';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Image } from 'react-native';
import { Entypo } from '@expo/vector-icons';
import axios from "axios"

export default function Router() {
  const Stack = createBottomTabNavigator();

  const [constructors, setConstructors] = useState([{}])
  const [drivers, setDrivers] = useState([{}])

  useEffect(() => {
    const getTeamsData = async () => {
      try {
        const res = await axios.get("http://ergast.com/api/f1/2022/constructorStandings.json")
        setConstructors(res.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings)
      } catch (err) {
        console.error(err)
      }
    }
    const getDriversData = async () => {
      try {
        const res = await axios.get("http://ergast.com/api/f1/2022/driverStandings.json")
        setDrivers(res.data.MRData.StandingsTable.StandingsLists[0].DriverStandings)
      } catch (err) {
        console.error(err)
      }
    }
    getDriversData()
    getTeamsData()
  }, [])

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={Home}
          options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({ focused, color, size }) =>
              focused ? (
                <Image
                  source={{ uri: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQoAAAC+CAMAAAD6ObEsAAAAe1BMVEX////hBgD2wcDsgH/oWFf85+b74+P1vr3iAADvlJPsfXzthIPrdHPeAAD0tbT1ubj++fnuion98fHseXj86+vzq6rxo6Lzr67wnJvpYWD3y8rqaGf63t3oU1L51dX529vmQT/iGxjqbmzlOjjlMi/mR0XjHxzkMjDoVlUAdP95AAACqklEQVR4nO3a63KiMBiA4WARI5oFqaJQj622vf8r3KA7nXbWz0MNYvR9/uIAeQ0giFIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHlHWni4W87LlUJLa9Q470tJ200P+T9Sa9JerQAdBYNwa2bW3xZW+Nj3wn6I81IGxEbSuUrikzR+7gba0WvPc9Nh/GL9Vu+w4wYkl4qYH/03W1XVl8KxEXtt88KxEtDQ1hvgqIWzjlkqMTY0dvCrRr3VKeFQiW9bZwacS7RUldua1HhtViUT5UaJT72nCoxIjSvyT13sRvZ8Sl9+RmY7yo8TkUAl716zXm/D1OX76tbilPC+hTVBMSkcPUuQST2424IBUQpu3xN1WfCjRleaEae0+MJyXo24vjuPX4jM83+dyrvw4OqQS5qVamiZ9bU8WQqyTHLl23Pyc0NvHj+kgMBc+vNBHrh23XyIo7cLxRRHupcTULiwu/wHqfYlAD+0d+/rSDj6VGAglVpm9bFwewqMSsVDiPTuw/3dZ4lko8ZFep0Sv6QBfpBJvD1fiSSixscuihyohnScer4R0dMzssqmDP8f8LxEqSuwU6lCJM+7JtiXE4+yGSvT3D2p7Kyp8k2bTTRaLqH0i+2NVRVJTD0r0lfRNmvD8B1k+z4nqXZfpvv3f3a+fyYcSL0KJ6lewUKI8fys+lCiEPewqYf9rKZForT/yIFWJGbod3+mkEhPldE4cP2OGUzXWiSp05nB053gR9jBXVy6hZnM16oVpMWtoVkjnieq0eN0SVYp8XAxam2ZSfB54n3S+f9EvSizE91a/nzE3NkXeMWrdSIoo2f8a8Xa4rX0vGXemv9jM3jVtff9UmakoUqUqUydjAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHBL/gLNZju1oC47fwAAAABJRU5ErkJggg==' }}
                  style={{
                    width: size,
                    height: size,
                    borderRadius: size
                  }}
                />
              ) : (
                <Entypo name="rocket" size={20} />
              )
          }}
        />
        <Stack.Screen
          name="Pilots"
          options={{
            tabBarIcon: ({ focused, color, size }) => (
              <Image
                source={{
                  uri:
                    'https://www.scottiesdesigns.com/wp-content/uploads/edd/2020/10/Racing-Helmet-SVG-Cricut-Silhouette.jpg'
                }}
                style={{
                  width: size,
                  height: size
                }}
              />
            )
          }}>
          {props => <Pilots {...props} drivers={drivers} />}
        </Stack.Screen>
        <Stack.Screen
          name="Teams"
          options={{
            tabBarIcon: ({ focused, color, size }) => {
              return (
                <Image
                  source={require('./assets/car2.png')}
                  style={{
                    width: size,
                    height: size
                  }}
                />
              );
            }
          }}>
          {props => <Teams {...props} constructors={constructors} />}
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
